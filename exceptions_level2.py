# exceptions - level 2 -
class FormulaError(Exception):
    pass


def interactive_calculator():
    """
    Function that gets a formula consisting of a number, an operator (at least + and -), and another number,
    separated by white space.
    :return: the result of the arithmetic practice.
    """
    try:
        users_input = input("Please insert your input, separated with spaces: ")
        exercise = [string.strip() for string in users_input.split()]
        exercise_len = len(exercise)
        if exercise_len == 3:
            operand1 = float(exercise[0])
            operator = exercise[1]
            operand2 = float(exercise[2])
            if operator == '+':
                result = operand1 + operand2
            elif operator == '-':
                result = operand1 - operand2
            else:
                raise FormulaError("Invalid operator. Only + and - are supported.")
            print(f"The result is: {result}")
        else:
            raise FormulaError("Invalid input. Please provide a valid formula.")
    except ValueError:
        print("Invalid input. Please enter valid numbers.")
    except FormulaError as error:
        print(f"Formula error - {error}")


if __name__ == "__main__":
    interactive_calculator()
