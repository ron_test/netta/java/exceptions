# exceptions - level 1 - 1
def zero_division_error():
    """
    function that gets num and his numerator.
    returns: if the numerator isn't valid.
    """
    num_str = input("Please enter the numerator: ")
    num = float(num_str)
    try:
        num_that_divides_str = input("Please enter the denominator: ")
        num_that_divides = float(num_that_divides_str)
        result = num / num_that_divides
        return result
    except Exception as error:
        print("Error: ", error)


result = zero_division_error()
if result is not None:
    print("Result:", result)


# exceptions - level 1 - 2
def valid_integer():
    """
    gets a num.
    returns: if there is a problem with the input.
    """
    try:
        num_input = int(input("please enter valid integer num: "))
        return print(num_input)
    except Exception as error:
        print("Eror: ", error)


valid_integer()


# exceptions - level 1 - 3
def file_not_found_error():
    """
    Function that prompts the user to enter a file name and attempts to open it.
    Prints a success message if the file is opened, otherwise handles the file not found error.
    """
    try:
        file_input = input("Please enter the file name: ")
        with open(file_input, 'r') as file:
            print("File open succeeded")
    except FileNotFoundError as error:
        print("Error: File not found -", error)


file_not_found_error()


# exceptions - level 1 - 4
def are_numerical():
    """
    function that gets 2 numbers.
    returns: if one of then isn't numerical.
    """
    try:
        num1_input = int(input("please enter valid integer num: "))
        num2_input = int(input("please enter valid integer num: "))
        return print(f"{num1_input} , {num2_input}")
    except Exception as error:
        print("Eror: ", error)


# exceptions - level 1 - 5
def permission_issue():
    """
    Function that prompts the user to enter a file name and attempts to open it.
    Prints a success message if the file is opened and the user has permission; otherwise, handles file not found or permission errors.
    """
    try:
        file_input = input("Please enter the file name: ")
        with open(file_input, 'r') as file:
            print("File open succeeded")
    except PermissionError as error:
        print("Error: You do not have permission to access the file -", error)


if __name__ == "__main__":
    permission_issue()


# exceptions - level 1 - 6
def index_is_out_of_range():
    """
    Function that gets a list and a wanted index.
    Returns if the index is out of range.
    """
    input_lst = input("Please enter your list, the objects should be separated by commas: ")
    list = [string.strip() for string in input_lst.split(",")]
    try:
        i = int(input("Please enter the index you want: "))
        print(f"The element at index {i} is: {list[i]}")
    except IndexError as error:
        print("Error: The index is out of range -", error)


if __name__ == "__main__":
    index_is_out_of_range()


# exceptions - level 1 - 7
def keyboard_interrupt():
    """
    function that gets num.
    returns: if the user cancels the input.
    """
    try:
        num_input = int(input("please enter num: "))
        return print(num_input)
    except KeyboardInterrupt as eror:
        print("Error: the user canceled the input- ",eror)


if __name__ == "__main__":
    keyboard_interrupt()


# exceptions - level 1 - 8
def arithmetic_error():
    """
    function that gets 2 numbers and calculates the division of them.
    returns: if thers an arithmetic error.
    """
    num1_input = int(input("please enter valid integer num: "))
    num2_input = int(input("please enter valid integer num: "))
    try:
        division = num1_input / num2_input
        return print(f"the division is: {division}")
    except ArithmeticError as eror:
        print("you have arithmetic error -", eror)


if __name__ == "__main__":
    arithmetic_error()


# exceptions - level 1 - 9
def unicode_decode_error():
    """
    function that opens a file.
    returns: if thers a problam with  an encoding issue.
    """
    try:
        file_input = input("Please enter the file name: ")
        with open(file_input, 'r') as file:
            return print("File open succeeded")
    except UnicodeDecodeError as error:
        print("Error: File has an encoding issue -", error)


if __name__ == "__main__":
    unicode_decode_error()


# exceptions - level 1 - 10
def attribute_does_not_exist():
    """
    function that gets a list.
    returns: if the attribute does not exist.
    """
    try:
        input_lst = input("Please enter your list, the objects should be separated by commas: ")
        list = [string.strip() for string in input_lst.split(",")]
        return print("the list: ", list)
    except AttributeError as eror:
        print("you have an attribute error -", eror)


if __name__ == "__main__":
    attribute_does_not_exist()

